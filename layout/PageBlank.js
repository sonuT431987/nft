
import React from 'react';

const pageBlank = ({children}) => {
  return (
    <>
        {children}
    </>
  );
}

export default pageBlank;